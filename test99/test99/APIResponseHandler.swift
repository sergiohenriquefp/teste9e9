//
//  APIResponseHandler.swift
//
//  Created by Sergio Freire on 24/06/15.
//  Copyright (c) 2015 Sergio Freire. All rights reserved.
//

import Foundation

class APIResponseHandler {
    
    class func onServerResponse(request : NSURLRequest, response : NSHTTPURLResponse?,  obj: AnyObject?, error : NSError?, onSuccess: (result: AnyObject?) -> Void, onError: (messages : [String]) -> Void) {
        
        var arrayMessages = [String]()
        
        if error != nil {
            arrayMessages.append("Problemas ao se comunicar com o serviço...")
            onError(messages: arrayMessages)
            return
        }
        
        
        if response?.statusCode == 200{
            
            if let result = obj as? NSArray{
                
                onSuccess(result: obj);
                return
            }
            
        }
        
        arrayMessages.append("Problemas ao se comunicar com o serviço...")
        onError(messages: arrayMessages)
        return
    }
}
