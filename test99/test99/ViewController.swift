//
//  ViewController.swift
//  test99
//
//  Created by Sergio Freire on 22/07/15.
//  Copyright (c) 2015 Sergio Freire. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class ViewController : UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var btRefresh: UIButton!
    @IBOutlet weak var aiIndicator: UIActivityIndicatorView!
    
    var locationManager:CLLocationManager!
    
    var drivers : [Driver]?
    
    var arrayPins = [CustomAnnotation]()
    var originPin = CustomAnnotation()
    
    var hasAddedOrigin = false
    var firstTimeZoom = true
    var isUsingService = false
    
    override func viewDidLoad()
    {
        
        super.viewDidLoad()
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        
        var timer = NSTimer.scheduledTimerWithTimeInterval(15.0, target: self, selector: Selector("obterDrivers"), userInfo: nil, repeats: true)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.Default
    }
    
    func locationManager(manager: CLLocationManager!, didUpdateToLocation newLocation: CLLocation!, fromLocation oldLocation: CLLocation!) {
        
        originPin.coordinate = CLLocationCoordinate2DMake(newLocation.coordinate.latitude, newLocation.coordinate.longitude)
        
        if !hasAddedOrigin{
            mapView.addAnnotation(originPin)
            hasAddedOrigin = true
            originPin.title = "Você"
            originPin.subtitle = "Este é você!"
            originPin.imageName = "painel_maps_origem.png"
        }
        
        if firstTimeZoom {
        
            firstTimeZoom = false
            
            var lat:CLLocationDegrees = originPin.coordinate.latitude
            var long:CLLocationDegrees = originPin.coordinate.longitude
            var latDelta:CLLocationDegrees = 0.02
            var longDelta:CLLocationDegrees = 0.02
            
            var span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, longDelta)
            var location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(lat, long)
            var region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
            
            mapView.setRegion(region, animated: true)
            
            self.obterDrivers()
        }
    }
    
    @IBAction func zoomAction(sender: AnyObject) {
        
        if hasAddedOrigin{
            var lat:CLLocationDegrees = originPin.coordinate.latitude
            var long:CLLocationDegrees = originPin.coordinate.longitude
            var latDelta:CLLocationDegrees = 0.02
            var longDelta:CLLocationDegrees = 0.02
            
            var span:MKCoordinateSpan = MKCoordinateSpanMake(latDelta, longDelta)
            var location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(lat, long)
            var region:MKCoordinateRegion = MKCoordinateRegionMake(location, span)
            
            
            mapView.setRegion(region, animated: true)
            
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    @IBAction func obterDriversAction(sender: AnyObject) {
        
        self.obterDrivers()
    }
    
    func obterDrivers(){
        
        var mapRect = self.mapView.visibleMapRect
        
        var cornerPointNE = MKMapPointMake(mapRect.origin.x + mapRect.size.width, mapRect.origin.y)
        var cornerCoordinateNE = MKCoordinateForMapPoint(cornerPointNE)
        
        var cornerPointSW = MKMapPointMake(mapRect.origin.x, mapRect.origin.y + mapRect.size.height)
        var cornerCoordinateSW = MKCoordinateForMapPoint(cornerPointSW)
        
        if !self.isUsingService && isConnected(showAlert: true) {
            
            self.isUsingService = true
            self.aiIndicator.startAnimating()
            
            API.sharedInstance().findDrivers(cornerCoordinateSW, ne: cornerCoordinateNE, onSuccess: { (result) -> Void in
                
                self.parseSuccessResult(result)
                self.aiIndicator.stopAnimating()
                self.isUsingService = false
                }, onError: { (message) -> Void in
                    self.showMessage(message[0])
                    self.aiIndicator.stopAnimating()
                    
                    
            })
        }
    }
    
    func parseSuccessResult(obj : AnyObject?){
        
        if let result = obj as? NSArray{
            
            self.drivers = DriverModel.getListJson(result);
            
            filterAnnotations()
        }
    }
    
    func filterAnnotations(){
    
        if self.drivers != nil {
        
            var latDelta = self.mapView.region.span.latitudeDelta/90.0;
            var longDelta = self.mapView.region.span.longitudeDelta/90.0;
            
            var annotationsShow=[Driver]()
            
            for checkingLocation in self.drivers! as [Driver] {
                
                var latitude = checkingLocation.latitude!;
                var longitude = checkingLocation.longitude!;
                
                var found = false;
                
                for tempPlacemark in annotationsShow as [Driver] {
                    
                    if fabs(tempPlacemark.latitude! - latitude) < latDelta && fabs(tempPlacemark.longitude! - longitude) < longDelta {
                        self.removeAnnottationWith(checkingLocation.driverId!)
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    
                    if let driverAnnotation = getAnnotation(checkingLocation.driverId!){
                    
                        driverAnnotation.coordinate = CLLocationCoordinate2DMake(checkingLocation.latitude!, checkingLocation.longitude!)
                        if !checkingLocation.driverAvailable!{
                            driverAnnotation.title = "Ocupado (" + String(checkingLocation.driverId!) + ")"
                            driverAnnotation.imageName = "painel_maps_destino.png"
                        }
                        else{
                            driverAnnotation.title = "Livre (" + String(checkingLocation.driverId!) + ")"
                            driverAnnotation.imageName = "painel_maps_ondeestou.png"
                        }
                    }
                    else{
                    
                        var driverAnnotation = CustomAnnotation()
                        
                        driverAnnotation.coordinate = CLLocationCoordinate2DMake(checkingLocation.latitude!, checkingLocation.longitude!)
                        
                        if !checkingLocation.driverAvailable!{
                            driverAnnotation.title = "Ocupado (" + String(checkingLocation.driverId!) + ")"
                            driverAnnotation.imageName = "painel_maps_destino.png"
                        }
                        else{
                            driverAnnotation.title = "Livre (" + String(checkingLocation.driverId!) + ")"
                            driverAnnotation.imageName = "painel_maps_ondeestou.png"
                        }
                        driverAnnotation.driverId = checkingLocation.driverId!
                        
                        self.arrayPins.append(driverAnnotation)
                        annotationsShow.append(checkingLocation)
                        self.mapView.addAnnotation(driverAnnotation)
                    }
                }
            }
            
            self.removeNotUsed()
        }
        
    }
    
    func getAnnotation(idDriver: Int) -> CustomAnnotation?{
    
        for annotation in self.arrayPins as [CustomAnnotation] {
            
            if annotation.driverId == idDriver{
                
                return annotation
            }
        }
        
        return nil
    }
    
    func removeNotUsed(){
    
        for annotationOld in self.arrayPins as [CustomAnnotation] {
            
            var found = false;
            
            for checkingLocation in self.drivers! as [Driver] {
                
                if annotationOld.driverId == checkingLocation.driverId!{
                    found = true;
                    break;
                }
            }
            if (!found) {
                
                removeAnnottationWith(annotationOld.driverId)
            }
        }
        
        
    }
    
    func removeAnnottationWith(idDriver: Int){
    
        var i = 0;
        var index = -1;
        
        for annotation in self.arrayPins as [CustomAnnotation] {
        
            if annotation.driverId == idDriver{
            
                self.mapView.removeAnnotation(annotation)
                index = i;
            }
            
            i++
        }
        
        if index > 0 {
        
            self.arrayPins.removeAtIndex(index)
            
        }
    }
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        if !(annotation is CustomAnnotation) {
            return nil
        }
        
        let reuseId = "test"
        
        var anView = mapView.dequeueReusableAnnotationViewWithIdentifier(reuseId)
        if anView == nil {
            anView = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            anView.canShowCallout = true
        }
        else {
            anView.annotation = annotation
        }
        
        let cpa = annotation as! CustomAnnotation
        anView.image = UIImage(named:cpa.imageName)
        anView.alpha = 0
        
        return anView
    }
    
    func mapView(mapView: MKMapView!, regionDidChangeAnimated animated: Bool) {
        
        filterAnnotations()
        self.obterDrivers()
    }
    
    func mapView(mapView: MKMapView!, didAddAnnotationViews views: [AnyObject]!) {
        
        for annView in views as! [MKAnnotationView] {
            
            UIView.animateWithDuration(1.0,
                delay: 0.0,
                options: .CurveEaseInOut,
                animations: {
                    
                    annView.alpha = 1.0
                },
                completion: { finished in
                    annView.alpha = 1.0
            })
        }
    }
    
    func showMessage(text: String){
        
        var alert = UIAlertController(title: "Oops!", message: text, preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Tentar Novamente", style: .Default, handler: { action in
            switch action.style{
                
            case .Default:
                self.obterDrivers()
                
            case .Cancel:
                println("cancel")
                
            case .Destructive:
                println("destructive")
            }
            
            self.isUsingService = false
        }))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func isConnected(showAlert alert: Bool) -> Bool {
        
        let reachability = Reachability.reachabilityForInternetConnection()
        
        if reachability.isReachable() == false && alert == true {
            showMessage("Sem Internet no momento.")
        }
        
        return reachability.isReachable()
    }
}


