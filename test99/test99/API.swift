//
//  API.swift
//
//  Created by Sergio Freire on 24/06/15.
//  Copyright (c) 2015 Sergio Freire. All rights reserved.
//

import Foundation
import MapKit

class API {
    
    //4d3389193c6104506f0208124860ae0d7f35e9191c093e1cb44e5cb22689b4f5
    //b06ccaa4b92337338e1addd573557dca058778311eda52c7cb56612df15f3ebe
    
    var series : [Driver]?
    
    class func sharedInstance() -> API! {
        struct Static {
            static var onceToken : dispatch_once_t = 0
            static var instance : API? = nil
        }
        
        dispatch_once(&Static.onceToken) {
            Static.instance = API()
        }
        
        return Static.instance!
    }
    
    init() {
        
    }
    
    //https://api.99taxis.com/lastLocations?sw=-23.612474,-46.702746&ne=-23.589548,-46.673392
    //https://api.99taxis.com/lastLocations?sw=-23.612474,-46.702746&ne=-23.589548,-46.673392
    //https://api.99taxis.com/lastLocations?sw=-42.100092,-74.794929&ne=17.192985,-34.189446
    
    func findDrivers(sw: CLLocationCoordinate2D, ne:CLLocationCoordinate2D, onSuccess: (result: AnyObject?) -> Void, onError: (messages : [String]) -> Void) {
        
        var finalString = "https://api.99taxis.com/lastLocations?sw=" + String(format:"%f", sw.latitude) + "," + String(format:"%f", sw.longitude)
        
        finalString = finalString + "&ne=" + String(format:"%f", ne.latitude)  + "," +  String(format:"%f", ne.longitude)
        
        request(.GET, finalString, parameters:nil, encoding: .URL)
            .responseJSON{(request, response, JSON, error) in
                
                APIResponseHandler.onServerResponse(request, response: response, obj: JSON, error: error, onSuccess: { (result) -> Void in
                    
                    onSuccess(result: result)
                    
                    }, onError: { (message) -> Void in
                        
                        onError(messages: message)
                })
        }
    }
}
