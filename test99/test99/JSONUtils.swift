//
//  JSONUtils.swift
//  transportadora
//
//  Created by Sergio Freire on 22/07/15.
//  Copyright (c) 2015 Sergio Freire. All rights reserved.
//

import Foundation

class JSONUtils {
    
    class func parseString(dictionary: NSDictionary, field: String, defaultValue: String) -> String{
        
        if let finalString = dictionary[field] as? String{
        
            return finalString
        }
        
        if let finalInt = dictionary[field] as? Int{
            
            return String(finalInt)
        }
        
        return defaultValue
    }
    
    class func parseInt(dictionary: NSDictionary, field: String, defaultValue: Int) -> Int{
        
        if let finalStringInt = dictionary[field] as? String{
            
            if let finalInt = finalStringInt.toInt(){
            
                return finalInt
            }
        }
        
        if let finalInt = dictionary[field] as? Int{
            
            return finalInt
        }
        
        return defaultValue
    }
    
    class func parseDouble(dictionary: NSDictionary, field: String, defaultValue: Double) -> Double{
        
        if let finalStringDouble = dictionary[field] as? String{
            
            return (finalStringDouble as NSString).doubleValue
        }
        
        if let finalDouble = dictionary[field] as? Double{
            
            return finalDouble
        }
        
        return defaultValue
    }
    
    class func parseBool(dictionary: NSDictionary, field: String, defaultValue: Bool) -> Bool{
        
        if let finalStringBool = dictionary[field] as? String{
            
            if JSONUtils.parseString(dictionary, field: field, defaultValue: "") == "0"{
                return false
            }
            else {
                return true
            }
        }
        
        if let finalBool = dictionary[field] as? Bool{
            
            return finalBool
        }
        
        if let finalIntegerBool = dictionary[field] as? Int{
            
            return finalIntegerBool == 1 ? true : false
        }
        
        return defaultValue
    }
}