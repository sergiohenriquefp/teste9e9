//
//  DriverModel.swift
//  test99
//
//  Created by Sergio Freire on 22/07/15.
//  Copyright (c) 2015 Sergio Freire. All rights reserved.
//

import Foundation

class Driver {
    
    var longitude : Double?
    var latitude : Double?
    var driverId : Int?
    var driverAvailable : Bool?
    var urlImage : String?
    
    init() {
        
    }
}

class DriverModel {
    
    class func getListJson(array: NSArray) -> [Driver]?{
        
        var arrayFinal = [Driver]()
        
        var i = 0
        
        for item in array as! [NSDictionary] {
            
            if let parsedObj = DriverModel.getObjectJson(item) {
                
                arrayFinal.append(parsedObj);
            }
            
            i++
        }
        
        return arrayFinal
    }
    
    class func getObjectJson(obj: NSDictionary) -> Driver?{
        
        NSLog("%@",obj)
        
        var parsedObj : Driver = Driver();
        
        parsedObj.latitude = JSONUtils.parseDouble(obj, field: "latitude", defaultValue: 0.0)
        parsedObj.longitude = JSONUtils.parseDouble(obj, field: "longitude", defaultValue: 0.0)
        parsedObj.driverId = JSONUtils.parseInt(obj, field: "driverId", defaultValue: 0)
        parsedObj.driverAvailable = JSONUtils.parseBool(obj, field: "driverAvailable", defaultValue: false)
        
        return parsedObj
    }
}