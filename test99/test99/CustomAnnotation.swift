//
//  CustomAnnotation.swift
//
//  Created by Sergio Freire on 22/07/15.
//  Copyright (c) 2015 Sergio Freire. All rights reserved.
//

import Foundation
import MapKit

class CustomAnnotation: MKPointAnnotation {
    var imageName: String!
    var driverId: Int!
}